﻿using System;
using System.Web.UI;

/// <summary>
///     The Code behind for the midterm's master
/// </summary>
/// <author>
///     Destiny Harris
/// </author>
/// <version>
///     Febuary 22, 2015
/// </version>
public partial class Midterm : MasterPage
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}