﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Midterm.master" AutoEventWireup="true" CodeFile="Results.aspx.cs" Inherits="Results" %>

<asp:Content ID="resultHeadSection" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Results.css" rel="stylesheet"/>
</asp:Content>

<asp:Content ID="reultsSubHeaderSection" ContentPlaceHolderID="subHeaderContent" Runat="Server">
    <div class="resultsHeadingLabel">Voting Results</div>
</asp:Content>

<asp:Content ID="resultsBodySection" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label class="descriptionLabel">Votes for P:</label>
    <asp:Label ID="lblPVotes" CssClass="countsLabel" runat="server" Text=""></asp:Label>
    <br/>

    <label class="descriptionLabel">Votes for Q:</label>
    <asp:Label ID="lblQVotes" CssClass="countsLabel" runat="server" Text=""></asp:Label>
    <br/>

    <label class="descriptionLabel">Totals Votes:</label>
    <asp:Label ID="lblTotalVotes" CssClass="countsLabel" runat="server" Text=""></asp:Label>
</asp:Content>