﻿using System;
using System.Web.UI;

/// <summary>
///     Code-Behind for viewing results.
/// </summary>
/// <author>
///     Destiny Harris
/// </author>
/// <version>
///     Febuary 21, 2015 | Spring
/// </version>
public partial class Results : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            return;
        }

        var pVotes = 0;
        var qVotes = 0;
        var totalVotes = 0;

        if (Session["PVoteCount"] != null)
        {
            pVotes = Convert.ToInt32(Session["PVoteCount"]);
        }

        if (Session["QVoteCount"] != null)
        {
            qVotes = Convert.ToInt32(Session["QVoteCount"]);
        }

        if (Session["Count"] != null)
        {
            totalVotes = Convert.ToInt32(Session["Count"]);
        }

        this.lblPVotes.Text = pVotes.ToString();
        this.lblQVotes.Text = qVotes.ToString();
        this.lblTotalVotes.Text = totalVotes.ToString();
    }
}