﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Midterm.master" AutoEventWireup="true" CodeFile="Vote.aspx.cs" Inherits="Vote" %>

<asp:Content ID="voteHeadSection" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Vote.css" rel="stylesheet"/>
</asp:Content>

<asp:Content ID="voteSubHeaderSection" ContentPlaceHolderID="subHeaderContent" Runat="Server">
    <div class="voteHeadingLabel">
        Poll for Ages 18 - 25
    </div>
</asp:Content>

<asp:Content ID="voteBodyContent" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label class="voteLabel">Age:</label>
    <asp:TextBox ID="txtAge" CssClass="voteText" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvAge" runat="server" ErrorMessage="You must enter your age." TabIndex="1" Display="Dynamic" ControlToValidate="txtAge"></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="rvAge" runat="server" ControlToValidate="txtAge" Display="Dynamic" ErrorMessage="Age must be 18 - 25." MaximumValue="25" MinimumValue="18" Type="Integer"></asp:RangeValidator>

    <label class="voteLabel">Your Choice:</label>
    <br/>
    <asp:RadioButton ID="rdoP" CssClass="voteRadio" GroupName="Choice" runat="server" Text="P"/>
    <asp:RadioButton ID="rdoQ" CssClass="voteRadio" GroupName="Choice" runat="server" Text="Q"/>
    <br/>
    <br/>

    <asp:Button ID="btnVote" CssClass="voteButton" runat="server" Text="Cast Vote" OnClick="btnVote_Click"/>

</asp:Content>