﻿using System;
using System.Web.UI;

/// <summary>
///     The cod behind for casting a vote
/// </summary>
/// <author>
///     Destiny Harris
/// </author>
/// <version>
///     Febuary 21, 2015 | Spring
/// </version>
public partial class Vote : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    ///     Handles the Click event of the btnVote control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnVote_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }

        var count = Convert.ToInt32(Session["Count"]);
        count++;
        Session["Count"] = count;

        var pVoteCount = Convert.ToInt32(Session["PVoteCount"]);
        var qVoteCount = Convert.ToInt32(Session["QVoteCount"]);

        if (this.rdoP.Checked)
        {
            pVoteCount++;
            Session["PVoteCount"] = pVoteCount;
        }

        if (this.rdoQ.Checked)
        {
            qVoteCount++;
            Session["QVoteCount"] = qVoteCount;
        }

        Response.Redirect("Results.aspx");
    }
}